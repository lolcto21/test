<?php

namespace App\Console\Commands;

use App\Employees;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ParseEmployeeFromJsonFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'employee:parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parses files and creates records to employees table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $files = Storage::disk('jsons')->allFiles();
        foreach ($files as $file) {
            $employees = json_decode(Storage::disk('jsons')->get($file), true);
            $department = Str::lower(str_replace('.json', '', $file));
            foreach ($employees as $employee) {
                $validator = Validator::make($employee, [
                    '_id' => 'required|string',
                    'start' => 'required|date|before:end',
                    'end' => 'required|date|after:start'
                ]);

                if ($validator->fails()) {
                    Log::error(__METHOD__ . ' Employee does not pass validation!');
                    Log::debug(__METHOD__, [
                        'data' => $employee,
                        'errors' => $validator->errors()
                    ]);
                    continue;
                }

                Employees::query()->updateOrInsert(['id' => $employee['_id']], [
                    'start_at' => $employee['start'],
                    'end_at' => $employee['end'],
                    'department' => $department,
                ]);
            }
        }
    }
}
