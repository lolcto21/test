<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\FormerEmployeeRequest;
use App\Services\EmployeeService;

class EmployeeController extends Controller
{
    /**
     * @param FormerEmployeeRequest $employeeRequest
     * @param EmployeeService $employeeService
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getFormerEmployeeStats(FormerEmployeeRequest $employeeRequest, EmployeeService $employeeService)
    {
        return response($employeeService->getFormerEmployeeStats($employeeRequest->get('department')));
    }

    /**
     * @param EmployeeService $employeeService
     * @return \Illuminate\Support\Collection
     */
    public function getDepartments(EmployeeService $employeeService)
    {
        return response($employeeService->getDepartments());
    }
}