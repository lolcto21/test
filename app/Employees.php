<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'department', 'start_at', 'end_at'
    ];

    protected $dates = [
        'start_at',
        'end_at'
    ];
}
