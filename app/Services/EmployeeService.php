<?php


namespace App\Services;


use App\Employees;
use Illuminate\Support\Facades\DB;

class EmployeeService
{

    public function getFormerEmployeeStats($department = null)
    {
        // select on which periods can be divided dataset
        $period = floor(Employees::query()
            ->select([DB::raw('MAX(TIMESTAMPDIFF(MONTH, employees.start_at, employees.end_at)) as max_work_time')])
            ->value('max_work_time') / 6);

        // get total count of records to calculate percentage
        $totalCount = Employees::query()->select(DB::raw('count(*) as total_count'))->value('total_count');

        //get records with month based periods
        $employeeStatsQuery = Employees::query()
            ->select([
                DB::raw("concat($period*floor(TIMESTAMPDIFF(MONTH, employees.start_at, employees.end_at)/$period), '-', $period*floor(TIMESTAMPDIFF(MONTH, employees.start_at, employees.end_at)/$period) + $period) as `range`"),
                'department'
            ]);

        //apply filter by department
        if(!is_null($department)) {
            $employeeStatsQuery->where('department', $department);
        }

        //group by records by department and range
        $stats = $employeeStatsQuery->get()->groupBy('department')->map(function ($departmentGroup) {
            return $departmentGroup->groupBy('range');
        });

        // calculate percentage
        return $stats->map(function ($departmentStats) use ($totalCount) {
            return $departmentStats->map(function ($employers) use ($totalCount) {
                return round(($employers->count() / $totalCount) * 100, 2);
            });
        });
    }

    public function getDepartments()
    {
        return Employees::query()
            ->select('department')
            ->groupBy('department')
            ->pluck('department');
    }
}